import java.util.Scanner;

/**
 * Escrever um algoritmo que receba a altura e 
 * a largura de um retângulo e calcule a sua área.
 */
public class Exercicio01 {


    public static void main(String[] args){


        Scanner leitor = new Scanner(System.in);

        System.out.println("Informe a altura");
        int altura = leitor.nextInt();

        System.out.println("Informe a largura");
        int largura = leitor.nextInt();

        int area = largura * altura;

        System.out.println("A área é: " + area);

        // String msg = "Olá mundo!";
        // if (args[0].equals("en")){
        //     msg = "Hello World!";
        // }
        
        // System.out.println(msg);
    }

}
